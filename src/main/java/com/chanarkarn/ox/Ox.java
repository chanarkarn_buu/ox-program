/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.ox;

import java.util.Scanner;

/**
 *
 * @author A_R_T
 */
public class Ox {
    public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		int row, col;
		boolean win = false;
		int turn = 0;
		int count = 0;
		System.out.println("Welcome to OX Game");
                
                String[][] board = new String[3][3];
		board[0][0] = "-";
		board[0][1] = "-";
		board[0][2] = "-";
		board[1][0] = "-";
		board[1][1] = "-";
		board[1][2] = "-";
		board[2][0] = "-";
		board[2][1] = "-";
		board[2][2] = "-";
		System.out.println("  1 2 3");
		for (int i = 0; i < board.length; i++) {
			System.out.print(i + 1 + " ");
			for (int j = 0; j < board.length; j++) {
				System.out.print(board[i][j] + " ");
			}
			System.out.println();
		}
                do{
                System.out.println("X Turn");
                System.out.println("Please input Row  Col : ");
			if (count % 2 == 0) {
				row = kb.nextInt();
				col = kb.nextInt();
				board[row - 1][col - 1] = "X";
				System.out.println("  1 2 3");
				for (int i = 0; i < board.length; i++) {
					System.out.print(i + 1 + " ");
					for (int j = 0; j < board.length; j++) {
					System.out.print(board[i][j] + " ");
					}
					System.out.println();
				}
				count++;
			}
                        if (board[0][0].equals("X") && board[0][1].equals("X") && board[0][2].equals("X")) {

				System.out.println("X Win...");
				break;
			}
			if (board[1][0].equals("X") && board[1][1].equals("X") && board[1][2].equals("X")) {

				System.out.println("X Win...");
				break;
			}
			if (board[2][0].equals("X") && board[2][1].equals("X") && board[2][2].equals("X")) {

				System.out.println("X Win...");
				break;
			}
			if (board[0][0].equals("X") && board[1][0].equals("X") && board[2][0].equals("X")) {

				System.out.println("X Win...");
				break;
			}
			if (board[0][1].equals("X") && board[1][1].equals("X") && board[2][1].equals("X")) {

				System.out.println("X Win...");
				break;
			}
			if (board[0][2].equals("X") && board[1][2].equals("X") && board[2][2].equals("X")) {

				System.out.println("X Win...");
				break;
			}
			if (board[0][0].equals("X") && board[1][1].equals("X") && board[2][2].equals("X")) {

				System.out.println("X Win...");
				break;
			}
			if (board[0][2].equals("X") && board[2][2].equals("X") && board[2][0].equals("X")) {

				System.out.println("X Win...");
				break;
			}
                        turn++;
			if (turn == 9) {
				win = true;
				System.out.println("Play Draw");
				break;
			}
			if (count % 2 != 0) {
				System.out.println("O Turn");
				System.out.println("Please input Row  Col : ");
				row = kb.nextInt();
				col = kb.nextInt();
				board[row - 1][col - 1] = "O";
				System.out.println("  1 2 3");
				for (int i = 0; i < board.length; i++) {
					System.out.print(i + 1 + " ");
					for (int j = 0; j < board.length; j++) {
						System.out.print(board[i][j] + " ");
					}
					System.out.println();
				}
				count++;
				if (board[0][0].equals("O") && board[0][1].equals("O") && board[0][2].equals("O")) {

					System.out.println("O Win...");
					break;
				}
				if (board[1][0].equals("O") && board[1][1].equals("O") && board[1][2].equals("O")) {

					System.out.println("O Win...");
					break;
				}
				if (board[2][0].equals("O") && board[2][1].equals("O") && board[2][2].equals("O")) {

					System.out.println("O Win...");
					break;
				}
				if (board[0][0].equals("O") && board[1][0].equals("O") && board[2][0].equals("O")) {

					System.out.println("O Win...");
					break;
				}
				if (board[0][1].equals("O") && board[1][1].equals("O") && board[2][1].equals("O")) {

					System.out.println("O Win...");
					break;
				}
				if (board[0][2].equals("O") && board[1][2].equals("O") && board[2][2].equals("O")) {

					System.out.println("O Win...");
					break;
				}
				if (board[0][0].equals("O") && board[1][1].equals("O") && board[2][2].equals("O")) {

					System.out.println("O Win...");
					break;
				}
				if (board[0][2].equals("O") && board[2][2].equals("O") && board[2][0].equals("O")) {

					System.out.println("O Win...");
					break;
				}
				turn++;
                        }
}while (win != true);

		System.out.println("Bye bye...");
                }
}
